package com.jiahuan.svgmapview.sample;

import android.graphics.Color;
import android.graphics.PointF;
import android.os.Bundle;
import android.support.v7.app.ActionBarActivity;

import com.jiahuan.svgmapview.SVGMapView;
import com.jiahuan.svgmapview.sample.helper.AssetsHelper;

import java.util.Random;

public class SparkActivity extends ActionBarActivity
{

    private SVGMapView mapView;

    @Override
    protected void onCreate(Bundle savedInstanceState)
    {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_spark);

        mapView = (SVGMapView) findViewById(R.id.spark_mapview);
        mapView.loadMap(AssetsHelper.getContent(this, "TA_P2.svg"));

        Random random = new Random();

        for (int i = 0; i < 1; i++)
        {
            int color = i % 2 == 0 ? Color.RED : Color.BLUE;
            mapView.getController().sparkAtPoint(new PointF(490, 277), 12, color, 100);
        }

        for (int i = 0; i < 1; i++)
        {
            //int color = i % 2 == 0 ? Color.RED : Color.BLUE;
            int color = Color.BLUE;
            mapView.getController().sparkAtPoint(new PointF(110, 360), 12, color, 100);
        }

    }
}

